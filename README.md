# DNA2BPM Vagrant VM

Copy your keys to home/.ssh


put whatever you want to be initially in www

copy a dump you want to be imported when provissioning 

###NOTE every time provission is made import is executed with --drop


Then in the directory where you cloned it 

```
vagrant up
```

when finished you wil have:
apache + mysql + mongo on http://localhost:8080
c9ide on http://localhost:9191 (developer developer)

mongodb on localhost:37017

if you want to pass some other files to the VM put them in their matching directory

Then:
```
vagrant provision
```

if you change anything on the vagrantfile:

```
vagrant reload
```

### host to host folder synced_folder 
if you want to have VM sites synced to your local filesystem (ie for using some advanced git tool as smartgit)

you can:

1 copy your site to www 

2 thru c9ide console or vagrant ssh make a soft link between VM www and synced www

```
ln -s /vagrant/www/yoursite
```
